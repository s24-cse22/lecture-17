#include <iostream>
using namespace std;

void print_header(string title){
    cout << ".---------------------------------------------------------------------------------------------------------------------------------------------------." << endl;
    cout << "| " << title;

    int spaces = 150 - 4 - title.length();

    for (int i = 0; i < spaces; i++){
        cout << " ";
    }

    cout << "|" << endl;

    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
    cout << "| Hole             |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  | OUT  | 10  | 11  | 12  | 13  | 14  | 15  | 16  | 17  | 18  |  IN  | TOT  |" << endl;
    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
}

void print_cell(int value){
    cout << " " << value;
    if (value < 100){
        cout << "  ";
    }
    else {
        cout << " ";
    }
    cout << "|";
}

void print_yardages(string color, int d1, int d2, int d3, int d4, int d5, int d6, int d7, int d8, int d9, int d10, int d11, int d12, int d13, int d14, int d15, int d16, int d17, int d18){
    cout << "| " << color;

    int spaces = 18 - 1 - color.length();

    for (int i = 0; i < spaces; i++){
        cout << " ";
    }
    cout << "|";

    print_cell(d1);
    print_cell(d2);
    print_cell(d3);
    print_cell(d4);
    print_cell(d5);
    print_cell(d6);
    print_cell(d7);
    print_cell(d8);
    print_cell(d9);

    int out = d1 + d2 + d3 + d4 + d5 + d6 + d7 + d8 + d9;
    print_cell(out);

    print_cell(d10);
    print_cell(d11);
    print_cell(d12);
    print_cell(d13);
    print_cell(d14);
    print_cell(d15);
    print_cell(d16);
    print_cell(d17);
    print_cell(d18);

    int in = d10 +d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18;
    print_cell(in);

    int total = in + out;
    print_cell(total);
    cout << endl;
    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
}

void print_blank(){
    cout << "|                  |     |     |     |     |     |     |     |     |     |      |     |     |     |     |     |     |     |     |     |      |      |" << endl;
    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
}

int main() {

    string name;
    getline(cin, name);
    print_header(name);

    int blue1, blue2, blue3, blue4, blue5, blue6, blue7, blue8, blue9, blue10, blue11, blue12, blue13, blue14, blue15, blue16, blue17, blue18;
    cin >> blue1 >> blue2 >> blue3 >> blue4 >> blue5 >> blue6 >> blue7 >> blue8 >> blue9 >> blue10 >> blue11 >> blue12 >> blue13 >> blue14 >> blue15 >> blue16 >> blue17 >> blue18;

    print_yardages("Blue", blue1, blue2, blue3, blue4, blue5, blue6, blue7, blue8, blue9, blue10, blue11, blue12, blue13, blue14, blue15, blue16, blue17, blue18);

    int white1, white2, white3, white4, white5, white6, white7, white8, white9, white10, white11, white12, white13, white14, white15, white16, white17, white18;
    cin >> white1 >> white2 >> white3 >> white4 >> white5 >> white6 >> white7 >> white8 >> white9 >> white10 >> white11 >> white12 >> white13 >> white14 >> white15 >> white16 >> white17 >> white18;

    print_yardages("White", white1, white2, white3, white4, white5, white6, white7, white8, white9, white10, white11, white12, white13, white14, white15, white16, white17, white18);

    int red1, red2, red3, red4, red5, red6, red7, red8, red9, red10, red11, red12, red13, red14, red15, red16, red17, red18;
    cin >> red1 >> red2 >> red3 >> red4 >> red5 >> red6 >> red7 >> red8 >> red9 >> red10 >> red11 >> red12 >> red13 >> red14 >> red15 >> red16 >> red17 >> red18;

    print_yardages("Red", red1, red2, red3, red4, red5, red6, red7, red8, red9, red10, red11, red12, red13, red14, red15, red16, red17, red18);

    print_blank();
    print_blank();
    print_blank();
    print_blank();

    return 0;
}